#pragma once

#include <iostream>
#include "Vector.h"

class Vector {

public:
	float X, Y, Z;
	Vector();
	Vector(float x, float y, float z);

	Vector Add(const Vector& v);
	Vector Subtract(const Vector& v);
	Vector MultiplyWithScalar(float scalar);

	float Length();
	float DistanceTo(Vector& v);

	Vector Normalize();
	Vector Negate();

	float DotProduct(const Vector& v);
	Vector CrossProduct(const Vector& v);
	float CalculateAngle(Vector& v);

	// Ylikirjoitetut operaattorit
	Vector operator+ (Vector& v);
	Vector operator- (Vector& v);
	Vector operator-();
	Vector operator* (float v);
};

float Length(Vector& v);
Vector Negate(Vector& v);

float DotProduct(Vector& v1, Vector& v2);
float DotProduct(float length1, float length2, float angle);

Vector CrossProduct(Vector& v1, Vector& v2);

float Distance(Vector& v1, Vector& v2);

float GetCosineOfAngle(float angle);
float GetAngleWithCosine(float cosVal);
float CalculateAngle(Vector& v1, Vector& v2);

Vector operator*(float f, Vector& v);

inline std::ostream& operator<< (std::ostream& out, const Vector& v) {
	out << "(" << v.X << ", "
		<< v.Y << ", "
		<< v.Z << ")";
	return out;
}
