#define _USE_MATH_DEFINES

#include "Vector.h"
#include <cmath>

Vector::Vector(float x, float y, float z) {
	X = x;
	Y = y;
	Z = z;
}

Vector::Vector() {
	X = 0;
	Y = 0;
	Z = 0;
}

Vector Vector::Add(const Vector& v) {
	Vector result;
	result.X = X + v.X;
	result.Y = Y + v.Y;
	result.Z = Z + v.Z;
	return result;
}

Vector Vector::Subtract(const Vector& v) {
	Vector result;
	result.X = X - v.X;
	result.Y = Y - v.Y;
	result.Z = Z - v.Z;
	return result;
}

Vector Vector::MultiplyWithScalar(float scalar) {
	Vector result;
	result.X = X * scalar;
	result.Y = Y * scalar;
	result.Z = Z * scalar;
	return result;
}

float Vector::Length() {
	float result;
	result = sqrt(X * X + Y * Y + Z * Z);
	return result;
}

Vector Vector::Normalize() {
	float length = Length();
	// Emme pysty jakamaan nollalla, palautetaan nollavektori
	if(length == 0)	return Vector(0, 0, 0);
	X = X / length;
	Y = Y / length;
	Z = Z / length;
	return *this;
}

Vector Vector::Negate() {
	Vector result;
	result.X = X * -1;
	result.Y = Y * -1;
	result.Z = Z * -1;
	return result;
}

float Length(Vector& v) {
	float result;
	result = sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z);
	return result;
}

Vector Negate(Vector& v) {
	Vector result;
	result.X = v.X * -1;
	result.Y = v.Y * -1;
	result.Z = v.Z * -1;
	return result;
}

float Vector::DistanceTo(Vector& v) {
	Vector newVector(v - *this);
	return ::Length(newVector);
}

float Distance(Vector& v1, Vector& v2) {
	return v1.DistanceTo(v2);
}

float Vector::DotProduct(const Vector& v) {
	return X * v.X + Y * v.Y + Z * v.Z;
}

float DotProduct(Vector& v1, Vector& v2) {
	return v1.DotProduct(v2);
}

float DotProduct(float length1, float length2, float angle) {
	return length1 * length2 * GetCosineOfAngle(angle);
}

float GetCosineOfAngle(float angle) {
	return cos(angle * M_PI / 180.0f);
}

float GetAngleWithCosine(float cosVal) {
	return acos(cosVal) * 180.0 / M_PI;
}

Vector Vector::CrossProduct(const Vector& v) {
	Vector result;
	result.X = Y * v.Z - Z * v.Y;
	result.Y = Z * v.X - X * v.Z;
	result.Z = X * v.Y - Y * v.X;
	return result;
}

Vector CrossProduct(Vector& v1, Vector& v2) {
	return v1.CrossProduct(v2);
}

float Vector::CalculateAngle(Vector& v) {
	float dotproduct = this->DotProduct(v);
	float length1 = this->Length();
	float length2 = v.Length();
	float cosVal = dotproduct / (length1 * length2);
	float angle = GetAngleWithCosine(cosVal);
	return angle;
}

float CalculateAngle(Vector& v1, Vector& v2) {
	return v1.CalculateAngle(v2);
}

// Operaattoreiden ylikirjoitus
Vector Vector::operator+ (Vector& v) {
	return Add(v);
}

Vector Vector::operator- (Vector& v) {
	return Subtract(v);
}

Vector Vector::operator-() {
	return Negate();
}

Vector Vector::operator* (float f) {
	return MultiplyWithScalar(f);
}

Vector operator*(float f, Vector& v) {
	return v.MultiplyWithScalar(f);
}