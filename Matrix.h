#pragma once
#include <stdexcept>
#include <iostream>

template <class T, int size>
class Matrix {
public:
	T matrix[size][size] = { 0 };

	Matrix() {}

	Matrix(T value) {
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				if(row == column) {
					matrix[row][column] = value;
				}
			}
		}
	}

	Matrix Transpose() {
		Matrix<T, size> result;
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				result.matrix[row][column] = matrix[column][row];
			}
		}
		return result;
	}

	Matrix<T, size - 1> RemoveRowAndColumn(int removedRow, int removedColumn) {
		Matrix<T, size - 1> newMatrix;
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				int wantedRow = row;
				int wantedColumn = column;

				if(wantedRow == removedRow || wantedColumn == removedColumn) continue;
				if(row > removedRow) wantedRow = row - 1;
				if(column > removedColumn) wantedColumn = column - 1;

				newMatrix.matrix[wantedRow][wantedColumn] = matrix[row][column];
			}
		}
		return newMatrix;
	}

	Matrix<T, size> Adjunct() {
		Matrix<T, size> adj;
		Matrix<T, size - 1> submatrix;
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				submatrix = RemoveRowAndColumn(row, column);
				adj.matrix[row][column] = Det(submatrix) * pow((-1), row + column);
			}
		}
		adj = adj.Transpose();
		return adj;
	}

	Matrix<T, size> Inverse() {
		float det = Det(*this);
		try {
			if(det == 0) {
				throw std::runtime_error("Ei ole mahdollista laskea käänteismatriisia, koska determinantti on 0.");
			}
		} catch(std::runtime_error& e) {
			std::cout << e.what() << "\n";
		}
		Matrix<T, size> adjunct = Adjunct();
		Matrix<T, size> inverse = adjunct / det;
		return inverse;

	}

	T& operator()(int i, int j) {
		return matrix[i][j];
	}

	Matrix<T, size> operator+(Matrix& m) {
		Matrix<T, size> result;
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				result.matrix[row][column] = matrix[row][column] + m.matrix[row][column];
			}
		}
		return result;
	}

	Matrix<T, size> operator-(Matrix& m) {
		Matrix<T, size> result;
		for(int row = 0; row < size; row++) {
			for(int column = 0; column < size; column++) {
				result.matrix[row][column] = matrix[row][column] - m.matrix[row][column];
			}
		}
		return result;
	}

	Matrix<T, size> operator*(Matrix& m) {
		Matrix<T, size> result;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					result.matrix[i][j] = result.matrix[i][j] + matrix[i][k] * m.matrix[k][j];
				}
			}
		}
		return result;
	}

	Matrix<T, size> operator*(T value) {
		Matrix<T, size> result;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					result.matrix[i][j] = matrix[i][j] * value;
				}
			}
		}
		return result;
	}

	Matrix<T, size> operator/(Matrix& m) {
		Matrix<T, size> result;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					result.matrix[i][j] = result.matrix[i][j] + matrix[i][k] / m.matrix[k][j];
				}
			}
		}
		return result;
	}

	Matrix<T, size> operator/(T value) {
		Matrix<T, size> result;
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					result.matrix[i][j] = matrix[i][j] / value;
				}
			}
		}
		return result;
	}
};

template <class T>
T Det(Matrix<T, 1>& m) {
	return m(0, 0);
}

template <class T>
float Det(Matrix<T, 2>& m) {
	return m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1);
}

template <class T, int size>
float Det(Matrix<T, size>& m) {
	float result = 0;
	for(int column = 0; column < size; column++) {
		Matrix<T, size - 1> submatrix = m.RemoveRowAndColumn(0, column);
		result += m(0, column) * pow((-1), 0 + column) * Det(submatrix);
	}
	return result;
}

template <class T, int size>
Matrix<T, size> operator*(T value, Matrix<T, size>& m) {
	return m * value;
}

template <class T, int size>
inline std::ostream& operator<< (std::ostream& out, const Matrix<T, size>& m) {
	out << "\n";
	for(int row = 0; row < size; row++) {
		for(int column = 0; column < size; column++) {
			out << m.matrix[row][column] << " ";
		}
		out << "\n";
	}
	return out;
}